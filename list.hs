{-# LANGUAGE OverloadedStrings #-}

import Brick
import Brick.Widgets.List
import Brick.Widgets.Center
  ( center
  , hCenter
  )
import Brick.Widgets.Border


import Control.Monad.IO.Class

import Graphics.Vty -- (mkVty)
import Control.Monad
import Data.Vector hiding (map)
import qualified Data.List as L


import Data.Text (pack)
import System.Process
import System.Directory -- (getDirectoryContents)


data Name = Name String deriving (Eq, Ord, Show)

type Dir = String

data State = State (List String Name) Dir


drawUI :: State -> [Widget String]
drawUI (State state dir) = [center $ ui ]
	where ui = hCenter $
		   vLimit 15 $
		   hLimit 50 $
		   borderWithLabel (txt (pack  dir)) $
		   renderList drawListElement True state

drawListElement :: Bool -> Name -> Widget String
drawListElement selected (Name name ) = 
      let color = if selected then withAttr selectedAttr else id
      in color $ str name

selectedAttr :: AttrName
selectedAttr = "selected"

theMap :: AttrMap
theMap = attrMap defAttr
  [ (selectedAttr, white `on` blue) ]

--appEvent :: State -> BrickEvent Name e -> EventM Name (Next State)
appEvent (State state dir) (VtyEvent e) =
  case e of
    EvKey (KChar 'q') [] -> halt (State state dir)
    EvKey (KChar 'k') [] -> continue $ (State (listMoveUp state) dir) 
    EvKey KUp [] -> continue $ (State ( listMoveUp state) dir)
    EvKey (KChar 'j') [] -> continue $ (State (listMoveDown state) dir)
    EvKey KDown [] -> continue $ (State (listMoveDown state) dir)
    _ -> continue (State state dir)

app :: App State e String --Name
app = App { appDraw = drawUI
          , appChooseCursor = showFirstCursor
          , appHandleEvent = appEvent
          , appStartEvent = return
          , appAttrMap = const theMap
          }

main :: IO ()
main = do
  (_, _,_) <- readProcessWithExitCode "nano" ["-l"] ""
  myFiles <- giveFileNames
  void $ defaultMain app myFiles 

names = list "names" ( fromList  [Name "one", Name "two"]) 1

giveFileNames = do
	  files <- L.sort <$> getDirectoryContents "."
          print $ files
	  curDir <- getCurrentDirectory
	  let nameFiles = State (list "files"  (fromList (map  Name files ) )   1) curDir
	  return nameFiles
