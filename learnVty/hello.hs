import Graphics.Vty

main = do
  cfg <- standardIOConfig
  vty <- mkVty cfg
  let line = string (defAttr `withForeColor` green) "rgv"
  update vty  (picForImage line)
  shutdown vty
--  print "rgv"
