{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
--module Main where

import qualified ModalEditor as E

import Lens.Micro
import Lens.Micro.TH
import Lens.Micro.Mtl
import qualified Graphics.Vty as V
import System.Environment
import qualified Brick.Main as M
import qualified Brick.Types as T
import qualified Data.Text.IO as TIO
import Data.Text (Text,unpack,pack)
import Brick.Widgets.Core
  ( (<+>)
  , (<=>)
  , hLimit
  , vLimit
  , str
  , vBox
  , withAttr
  , withDefAttr
  , viewport
  , visible
  )
import qualified Brick.Widgets.Center as C
--import qualified Brick.Widgets.Edit as E
import qualified Brick.AttrMap as A
import qualified Brick.Focus as F
import Brick.Util (on,fg)
import Control.Monad
import Data.Maybe
import Path
import Path.IO

import qualified Data.Text.Zipper as Z hiding (textZipper)
import qualified Data.Text.Zipper.Generic as Z
import qualified Data.Text.Zipper.Generic.Words as Z

data Name = Edit1
          | Edit2
          | EditLines
          deriving (Ord, Show, Eq)

data Mode = Normal | Insert | Visual deriving (Eq,Show)

type Pos = (Int,Int)

data St =
    St { _focusRing :: F.FocusRing Name
       , _edit1 :: E.Editor String Name
       , _edit2 :: E.Editor String Name
       , _mode :: Mode
       , _pos :: Maybe Pos
    }

makeLenses ''St

drawUI :: St -> [T.Widget Name]
drawUI st = [ui]
    where
        e1 =   F.withFocusRing (st^.focusRing) (E.renderEditor myRenderer)  (st^.edit1)
 --       e2 = F.withFocusRing (st^.focusRing) (E.renderEditor (str . unlines)) (st^.edit2)

        ui = C.center $ vBox $ [myModeline,(vLimit 90 resultEditor)]
        lineNumbersVp = renderWithLineNumbers $ st ^. edit1
        resultEditor = lineNumbersVp <+> e1
        myModeline = withAttr modeline $  (str $ show $ st ^. mode) <+> str " " <+> (str $ show $ st ^. pos) <+> str "   " <+>  (str $ show $ E.getCursorPosition $ st ^. edit1)
           -- (str "Input 1 (unlimited): " <+> vLimit 90 e1) -- (hLimit 130 $ vLimit 5 e1))  -- <=>
  --          str " " <=>
  --          (str "Input 2 (limited to 2 lines): " <+> (hLimit 30 e2)) <=>
  --          str " " <=>
  --          str "Press Tab to switch between editors, Esc to quit."

--myRenderer :: (Monoid t,Z.GenericTextZipper t) => [t] -> T.Widget n
myRenderer = -- str . Z.getText --  Z.currentLine
  str . unlines 
 {-
  body
  where body = withDefAttr curLineAttr $ vBox numWidgets
        maybeHighlight line
            | line == curLine

-}
deleteLeftMark :: (Eq t,Monoid t) => Pos  -> Z.TextZipper t -> Z.TextZipper t
deleteLeftMark dest content
	| dest > source = error "Impossible to parse"
	| dest == source = content
	| otherwise = deleteLeftMark dest  result
	where result = Z.deletePrevChar content
	      source = Z.cursorPosition content

deleteRightMark :: (Monoid t) => Pos -> Z.TextZipper t -> Z.TextZipper t
deleteRightMark dest content
	| dest < source = error "Impossible to parse"
	| dest == source = content
	| otherwise = deleteRightMark dest result
	where result = Z.deleteChar content
	      source = Z.cursorPosition content

normalAppEvent :: T.BrickEvent Name e  -> T.EventM Name St ()
--normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'q') [])) =
--   M.halt
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'c') [V.MCtrl])) = M.halt
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'x') [V.MCtrl])) = edit1 %= E.applyEdit Z.moveRight
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'k') [])) = do
  edit1 %= E.applyEdit Z.moveUp
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'l') [])) = do
  edit1 %= E.applyEdit Z.moveRight
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'h') [])) = do
  edit1 %= E.applyEdit Z.moveLeft
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'j') [])) = do
  edit1 %= E.applyEdit Z.moveDown
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'q') [])) =
   M.halt
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'i') [])) =
    mode %= const Insert
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'w') [])) =
  edit1 %= E.applyEdit Z.moveWordRight
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'b') [])) =
  edit1 %= E.applyEdit Z.moveWordLeft
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'v') [])) = do
	e <- use edit1
	m <- use mode
	if m == Normal then do
		mode %= const Visual
		pos %=  (\_ ->  Just $ E.getCursorPosition $ e)
	else do
		mode %= const Normal
		pos %= const Nothing

--  edit1 %= E.applyEdit Z.moveWordLeft
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'd') [])) = do
	markedPos <- use pos
	e <- use edit1
	let curPos = E.getCursorPosition e
	edit1 %= case markedPos of
		Nothing ->  E.applyEdit Z.deleteChar
		Just markedPos' ->
			if markedPos' > curPos
				then
				E.applyEdit ((deleteLeftMark curPos) . (Z.moveCursor markedPos'))
				else
				E.applyEdit  ( deleteLeftMark markedPos')
	pos %= const Nothing

--	let markedPos' =if markedPos

{-
normalAppEvent (T.VtyEvent (V.EvKey (V.KChar 'e') [])) = do
	curPos <- use pos
	e <- use edit1
	let curPos' = fromJust curPos
	return ()

-}

appEvent :: T.BrickEvent Name e -> T.EventM Name St ()
appEvent (T.VtyEvent (V.EvKey V.KEsc [])) =
    mode %= const Normal
--appEvent (T.VtyEvent (V.EvKey (V.KChar 'c') [V.MCtrl])) = M.halt
appEvent  e = do
  m <- use mode
  case m of
    Normal -> normalAppEvent e
    Visual -> normalAppEvent e
    Insert -> insertAppEvent e

insertAppEvent :: T.BrickEvent Name e -> T.EventM Name St ()
insertAppEvent (T.VtyEvent (V.EvKey (V.KChar 'c') [V.MCtrl])) = M.halt
insertAppEvent ev = do
    r <- use focusRing
    case F.focusGetCurrent r of
      Just Edit1 -> zoom edit1 $ E.handleEditorEvent ev
      Just Edit2 -> zoom edit2 $ E.handleEditorEvent ev
      Nothing -> return ()

renderWithLineNumbers :: E.Editor String Name -> T.Widget Name
renderWithLineNumbers e = hLimit (maxNumWidth + 1) $ viewport EditLines T.Vertical body
  where body = withDefAttr lineNumberAttr $ vBox numWidgets
        mkNumWidget i = maybeVisible i $ str $ show i
        maybeVisible i
          | i == curLine + 1 = visible . withDefAttr currentLineNumberAttr
          | otherwise = id
        numbers = [1..h]
        numWidgets = mkNumWidget <$> numbers
        h = length . E.getEditContents $ e
        curLine = fst $ E.getCursorPosition e
        maxNumWidth = length . show $ h

theMap :: A.AttrMap
theMap = A.attrMap V.defAttr
     [ (E.editFocusedAttr,    V.brightWhite `on` V.black)
     , (E.editAttr,            V.black `on` V.yellow)
     , (modeline,          V.black `on` V.brightMagenta)
    , (lineNumberAttr,          fg V.cyan)
    , (currentLineNumberAttr,   V.defAttr `V.withStyle` V.bold)
    , (currentLineAttr,   V.defAttr `V.withStyle` V.bold)
     ]

currentLineAttr = A.attrName "curLine"

modeline :: A.AttrName
modeline = A.attrName "modeline"

lineNumberAttr :: A.AttrName
lineNumberAttr = A.attrName "lineNumber"

currentLineNumberAttr :: A.AttrName
currentLineNumberAttr = lineNumberAttr <> A.attrName "current"

theApp :: M.App St e Name
theApp =
    M.App { M.appDraw = drawUI
          , M.appChooseCursor = appCursor
          , M.appHandleEvent = appEvent
          , M.appStartEvent = return ()
          , M.appAttrMap = const theMap
          }

appCursor :: St -> [T.CursorLocation Name] -> Maybe (T.CursorLocation Name)
appCursor = F.focusRingCursor (^. focusRing)

main :: IO ()
main = do
    args <- getArgs
    case args of
	[] -> error "empty file"
	[fp] -> do
		path <- resolveFile' fp
		maybeContents <- forgivingAbsence $ TIO.readFile (fromAbsFile path)
                let contents = unpack $ fromMaybe "" maybeContents
                st <- M.defaultMain theApp (initialState contents)
                putStrLn "In input 1 you entered:\n"
                putStrLn $ unlines $ E.getEditContents $ st^.edit1
                let contents' = pack $ unlines $ E.getEditContents $ st^.edit1
                putStrLn "In input 2 you entered:\n"
                putStrLn $ unlines $ E.getEditContents $ st^.edit2
                --unless (contents == contents') $
                TIO.writeFile (fromAbsFile path) contents'

initialState :: String -> St
initialState contents =
    St (F.focusRing [Edit1, Edit2])
       (E.editor Edit1 Nothing contents)
       (E.editor Edit2 (Just 2) "")
       (Normal)
       (Nothing)
