{-# Language OverloadedStrings#-}


import Data.Maybe

import Path
import Path.IO
import System.Environment
import System.IO
import qualified Data.Text.IO as T

main = do
  [fileName] <- getArgs
  path <- resolveFile' fileName
  let fp = fromAbsFile path
  maybeContents <- forgivingAbsence $ T.readFile fp
  let contents = fromMaybe "" maybeContents
  commandKey <- getChar
  hFlush stdout
  if commandKey == 'j' then do
    putStrLn "in insert mode "
    newContentInLine <- T.getLine 
    hFlush stdout
    let newContent = contents <> "\n" <> newContentInLine
    T.writeFile fp newContent
    print newContent 
  else  putStrLn "you seem arrogant"
