{-# LANGUAGE OverloadedStrings #-}

import Graphics.Vty
import System.Environment

import qualified Data.Text.IO as TIO
import Data.Maybe

import Data.Text (Text,unpack,pack)
import Path
import Path.IO



main :: IO ()
main = do
    args <- getArgs
    case args of
	[] -> error "empty file"
	[fp] -> do
       		path <- resolveFile' fp
		maybeContents <- forgivingAbsence $ TIO.readFile (fromAbsFile path)
                let contents = unpack $ fromMaybe "" maybeContents
                -- Initialize Vty
                vty <- mkVty defaultConfig
                defOutput <- outputForConfig defaultConfig
                setCursorPos defOutput 0 0
                showCursor defOutput

                -- Create a new image with the string "Hello, world!"
                let helloImage = vertCat $ map (string (defAttr `withForeColor` yellow)) (lines contents) --"1\n2\n3\n"-- contents --"Hello, world!"

                -- Display the image
                update vty (picForImage helloImage)

                -- Wait for user input
                _ <- nextEvent vty

                -- Shutdown Vty
                shutdown vty
