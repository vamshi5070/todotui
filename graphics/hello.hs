import Graphics.Vty

main :: IO ()
main = do
  -- Initialize Vty
  vty <- mkVty defaultConfig

  -- Create a new image with the string "Hello, world!"
  let helloImage = string (defAttr `withForeColor` yellow) "Hello, world!"

  -- Display the image
  update vty (picForImage helloImage)

  -- Wait for user input
  _ <- nextEvent vty

  -- Shutdown Vty
  shutdown vty
