
import Lens.Micro
import Lens.Micro.Mtl
import Lens.Micro.TH

import Data.List.PointedList
import Data.Maybe
import Data.List.PointedList.Circular
import Data.DynamicState.Serializable
import Data.Binary

source = "xmonad is a great window manager"

main = do
--  let contents = fromMaybe "" $ fromList src
  putStrLn "Best point: "  
  print $ curPoint (makePc source)
  putStrLn "Bye for now!!"

makePc src =   case fromList src of
	Nothing -> error "Empty content"
	Just pointedList -> pointedList
  


--curPoint :: PointedList Char -> Char
curPoint pc = _focus pc

-- Convert the string to a pointed list
--strToPointedList :: String -> PointedList Char
--strToPointedList = fromList

-- Move the focus point left
--moveLeft :: PointedList Char -> PointedList Char
--moveLeft = movePrev

-- Move the focus point right
--moveRight :: PointedList Char -> PointedList Char
--moveRight = moveNext

-- Get the current focus point
--getFocus :: PointedList a -> a
--getFocus = focus

-- Example usage:
exampleString = "hello world"
--pointedList = strToPointedList exampleString

-- Initially, the focus point is at the start of the string
--focusPoint = getFocus pointedList
-- focusPoint = 'h'

-- Move the focus point to the right
--pointedList = moveRight pointedList
--focusPoint = getFocus pointedList
-- focusPoint = 'e'

-- Move the focus point to the left
--pointedList = moveLeft pointedList
--focusPoint = getFocus pointedList
-- focusPoint = 'h'
